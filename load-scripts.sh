#!/bin/bash

SCRIPT_FOLDER="$HOME/Scripts/default"

echo -e "\n[?] Scripts are loading..."

for f in $SCRIPT_FOLDER/*.sh; do
	source $f;
done

echo -e "[+] Done ! \n"
