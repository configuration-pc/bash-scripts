# Scripts

## Installation

Clone this repository inside your `$HOME` folder

```Shell
cd $HOME
git clone https://gitlab.com/configuration-pc/bash-scripts.git
```

Now edit your bashprofile to add the `load-scripts.sh`:

```Shell
echo "source $HOME/Scripts/load-scripts.sh" >> $HOME/.bashrc
```

## Usage

Add the script you want execute automaticaly in each bash loading inside the `$HOME/Scripts/default`

Exemple:

```Shell
ln $HOME/Scripts/<SCRIPT NAME> $HOME/Scripts/default/
```
